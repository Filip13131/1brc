import { createReadStream }   from "fs";
import { workerPoolInstance } from "./workerPool/workerPoolInstance.js"

function combineData(target: Map<string,number[]>, data: Map<string,number[]>): void{
    data.forEach((val, key)=>{
        if(target.has(key)){
            let targetValue = target.get(key);
            target.set(key, [Math.min(val[0], targetValue[0]), Math.max(val[1],targetValue[1]), val[2]+targetValue[2], val[3]+targetValue[3] ]);
        }else{
            target.set(key, val);
        }
    })

}
function resolveLine(line: string, target:Map<string, number[]>): void{
    let name : string = "";
    let temp : string | number= "";
    let j = 0;
    while(line[j]!==";"){
        name+=line[j];
        j++;
    }
    j++;
    while(j<line.length){
        temp+=line[j];
        j++;
    }
    temp = Number(temp);
    let curr: any;
    if(target.has(name)){
        curr = target.get(name);
    if (curr[0] > temp){
        curr[0] = temp; 
    }
    if (curr[1] < temp){
        curr[1] = temp; 
    }
    curr[2]+=temp;
    curr[3]++;
    target.set(name, curr);
    }
    else{
        target.set(name, [temp, temp, temp, 1])
    }
}
function resolveLefoverLines(lines: Map<number, string[]>, target: Map<string,number[]>): void {
    let linesArray = Array.from(lines).sort((a, b)=>(a[0]-b[0]));
    console.log(linesArray.length);
    resolveLine(linesArray[0][1][0], target);
    for ( let i =0; i< linesArray.length-1; i++){
        let line = linesArray[i][1][1]+linesArray[i+1][1][0];
        resolveLine(line, target);
    }
}
async function run() : Promise<any>{
    const processes = {readStream: false, workerPool: false};
    return new Promise((resolve, reject)=>{
        const readStream = createReadStream('../measurements.txt');
        const target = new Map();
        const leftoverLines : Map<number, string[]> = new Map();
        let i = 0;
        readStream.on('data', async (chunk)=>{
            const l=i;
            if(i%1000===0){
                console.log(`processing chunk no. ${i}...`);
            }
            workerPoolInstance.runTask(chunk, (err, res)=>{
                leftoverLines.set(l, [res.firstLine, res.lastLine]);
                combineData(target, res.result);
            });
            i++;
        })

        readStream.on('end', ()=>{
            processes.readStream = true;
            console.log("file has been read...");
        })

        
        workerPoolInstance.on('WorkerPoolFree', async ()=>{
            console.log('WorkerPoolFreeFromOutside');
            if(processes.readStream){
                resolveLefoverLines(leftoverLines, target);
                workerPoolInstance.close();
                resolve(target);
            }
        })


    })
}


async function main(){
    
    let start = performance.now();
    const something = await run();
    let solution = Array.from(something).sort();
    let result ="{"
    for (const entry of solution){
        result+=entry[0];
        result+="=";
        result+=entry[1][0];
        result+="/";
        result+=(entry[1][2]/entry[1][3]).toFixed(1);
        result+="/";
        result+=entry[1][1];
        result+=", ";
    }
    result= result.slice(0,-2)
    result+="}"
    let end = performance.now();
    console.log(end-start);
    console.log(result);
    let sum = 0;
    something.forEach((val)=>{
        sum+=val[3];
    })
    console.log(`This is a count of all lines acording to agregated data: ${sum}`);


}

main();
