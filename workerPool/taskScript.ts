import { parentPort } from 'node:worker_threads';

const result = new Map();
const decoder = new TextDecoder('utf-8');
parentPort.on('message', ({chunk} : {chunk : any}) => {
    let currName = "";
    let currValue = "";
    let firstLine = "";
    let i = 0;
    while(chunk[i]!==10){
        firstLine+=String.fromCharCode(chunk[i]);
        i++;
    }
    i++;
    while(chunk[i]){
        while(chunk[i]!==59 && chunk[i]){
            currName+= String.fromCharCode(chunk[i]);
            i++;
        }
        i++;
        while(chunk[i]!==10 && chunk[i]){
            currValue+= String.fromCharCode(chunk[i]);
            i++;
        }
        if (chunk[i]===10){
            let temp = Number(currValue);
            if(result.has(currName)){
                let curr = result.get(currName);
                if (curr[0] > temp){
                    curr[0] = temp; 
                }
                if (curr[1] < temp){
                    curr[1] = temp; 
                }
                curr[2]+=temp;
                curr[3]++;
                result.set(currName, curr);
            }
            else{
                result.set(currName, [temp, temp, temp, 1]);
            }
            currName="";
            currValue="";
        }
        i++;
    }
    let lastLine= currName;
    if(chunk[i-2]===59&&!chunk[i-1]){
        lastLine+=";";
    }
    if(currValue){
        lastLine+= ";"+ currValue;
    }
    
    

    parentPort.postMessage({firstLine, lastLine, result});
    result.clear();
});
